import java.util.*;

public class ProbDist {
    
    double[] probs;
    public ProbDist(double[] probs) { this.probs = probs; normalize(); }
    
    public void setProbs (double[] probs) { this.probs = probs; }
    
    public double[] getProbs () { return this.probs; }
     
    public void normalize() {  
        double sum = 0.0; for ( double d : this.probs ) { sum+=d; }
        for ( int i = 0; i < probs.length; i++ ) { probs[i] = probs[i] / sum; }
    }

    /* Source:
     *  http://stackoverflow.com/questions/6737283/weighted-randomness-in-java
     */ 

    public int weightedRandom(){

        // Compute the total weight of all items together
        double totalWeight = 0.0d;
        for ( double i : this.probs) { totalWeight += i; }
        // Now choose a random item
        int randomIndex = -1;
        double random = Math.random() * totalWeight;
        for (int i = 0; i < probs.length; ++i)
        {
            random -= probs[i];
            if (random <= 0.0d)
            {
                randomIndex = i;
                break;
            }
        }
        return randomIndex;
    }
}
