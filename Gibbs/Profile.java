import java.util.ArrayList;

public class Profile {
    ArrayList<Sequence> DNA;
    int l;
    int n;
    int t;
    SymbolSet symbols;
    char[][] matrix;
    char[][] alignmentMatrix;
    int[][] discreteProfile;
    double[][] p;
    double[] p_GS;

    public Profile( ArrayList<Sequence> DNA, int l ){
        this.DNA = DNA;
        this.symbols = DNA.get(0).getSymbolSet();
        this.l = l;
        this.n = DNA.get(0).length();
        this.t = DNA.size();
        this.matrix = new char[t][n];
        this.alignmentMatrix = new char[t][l]; 
        this.discreteProfile = new int[DNA.get(0).getSymbolSet().size()][l];
        this.p = new double[this.symbols.size()][l];
        this.p_GS = new double[n - l + 1];
        buildMatrix();
    }
   
    public char[][] getMatrix(){ return this.matrix; }
    public char[][] getAlignmentMatrix(){ return this.alignmentMatrix; }
    public int[][] getDiscreteProfile(){ return this.discreteProfile; }
    public double[][] getP(){ return this.p; }
    public double[] getP_GS(){ return this.p_GS; }
   
    /**
     * Builds a t x n matrix
     */
    
    public void buildMatrix() { 
        for ( int i = 0; i < t; i++) {
            for ( int j = 0; j < n; j++ ) {
                matrix[i][j] = DNA.get(i).get(j); 
            } 
        }    
    }

    public void buildProfile(int[] s, int goldSeq) {    
        buildAlignmentMatrix(s); 
        buildDiscreteProfile(goldSeq); 
        buildProbabilityProfile(1);
    }
    
    public void buildProfile(int[] s) {    
        buildAlignmentMatrix(s); 
        buildDiscreteProfile(); 
        buildProbabilityProfile();
    }
    
    /**
     * Builds a t x l matrix
     */

    public void buildAlignmentMatrix(int[] s){
     //   System.out.println("t: " + t + " s.length:" + s.length + " DNA.size():" + DNA.size() ); 
        if ( s.length != t || s.length != DNA.size() ) { 
            System.out.println(" Usage: # of starting indices must equal # of sequences");
            System.exit(0); 
        }
        for (int i = 0; i < s.length; i++ ) { 
            for ( int j = 0; j < l; j++ ) { 
                    alignmentMatrix[i][j] = DNA.get(i).get(j + s[i]); 
                } 
        }
    }
    
    /**
     * Builds a SymbolSetSize x l matrix of discrete values
     * Create a profile P from the l-mers in the remaining t - 1 sequences.  
     */
    
    public void buildDiscreteProfile(int goldSeq) { 
        char c; 
        for (int i = 0; i < t; i++) { 
            for (int j = 0; j < l; j++) {
                if ( i == goldSeq ) 
                    continue;
                c = alignmentMatrix[i][j]; 
                discreteProfile[symbols.getIndex(c)][j] += 1; 
            } 
        }
    }
    
    public void buildDiscreteProfile() { 
        int[][] newDiscrete = new int[symbols.size()][l];
        char c; 
        for (int i = 0; i < t; i++) { 
            for (int j = 0; j < l; j++) {
                c = alignmentMatrix[i][j]; 
                newDiscrete[symbols.getIndex(c)][j] += 1; 
            } 
        }
        this.discreteProfile = newDiscrete;
    }
    /**
     * Builds a SymbolSetSize x l matrix of probabilities; the prob distribution down a col 
     * Create a profile P from the l-mers in the remaining t - 1 sequences.  
     */

    public void buildProbabilityProfile(int foo){ 
        double dividend = t-1; 
        for (int i = 0; i < p.length; i++) { 
            for (int j = 0; j < l; j++){ 
                p[i][j] = discreteProfile[i][j] / dividend; 
            }
        }
    }
    
    public void buildProbabilityProfile(){ 
        double dividend = t; 
        for (int i = 0; i < p.length; i++) { 
            for (int j = 0; j < l; j++){ 
                 p[i][j] = discreteProfile[i][j] / dividend; 
            }
        }
    }

    public void buildP_GS(int goldenSeq) { 
        char c;
        int x;
        double y = 1.0;
        for ( int i = 0; i < n - l + 1; i++ ) {
            y = 1.0;
            for ( int j = 0; j < l; j++ ) {
                c = matrix[goldenSeq][j+i];
                x = symbols.getIndex(c);
                y *= p[x][j];               
            }
            p_GS[i] = y;
        }
    }

   //TODO: Implement toString method to print out matrix.
    public String toString(char[][] a) {
        String returnval = "";
        returnval += "\n";
        for (int i = 0; i < a.length; i++){
            returnval += i + "   ";
            for (int j = 0; j < a[0].length; j++){
                returnval += a[i][j] + "   ";
            }
          returnval += "\n";
        }
        return returnval;
    } 
   //TODO: Implement toString method to print out matrix.
    public String toString(int[][] a) {
        String returnval = "";
        returnval += "\n";
        for (int i = 0; i < a.length; i++){
            returnval += i + "   ";
            for (int j = 0; j < a[0].length; j++){
                returnval += a[i][j] + "   ";
            }
          returnval += "\n";
        }
        return returnval;
    } 
   
    //TODO: Implement toString method to print out matrix.
    public String toString(double[][] a) {
        String returnval = "";
        returnval += "\n";
        for (int i = 0; i < a.length; i++){
            returnval += i + "   ";
            for (int j = 0; j < a[0].length; j++){
                returnval += a[i][j] + "   ";
            }
          returnval += "\n";
        }
        return returnval;
    }
	
    public int hammingDistance(){
		int hammingDistance = 0;
        for ( int i = 0; i < l; i++ ) {
            int max = 0;
            for ( int j = 0; j < symbols.size(); j++) {
                if ( this.discreteProfile[j][i] > max ) 
                max = this.discreteProfile[j][i];  
            }   hammingDistance += (this.t - max);       
        }
		return hammingDistance;
    }
}
