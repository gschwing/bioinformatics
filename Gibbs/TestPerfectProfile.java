import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
public class TestPerfectProfile {
  // Arguments must be a location seed and a factory seed.
  public static void main(String[] args){

    final int NUMBER_OF_SEQUENCES = 20;
    final int MOTIF_LENGTH = 10;
    final int SEQUENCE_LENGTH = 500;
    
    Random random = new Random();
    random.setSeed(Long.parseLong(args[0]));

    RandomSequenceFactory rsf = new RandomSequenceFactory(new DNASymbolSet(), Long.parseLong(args[1]));
    Motif motif = new Motif("ACCACCATGA", new DNASymbolSet());
    ArrayList<Sequence> sequences = new ArrayList<Sequence>();
    int[] insertedIndices = new int[NUMBER_OF_SEQUENCES];
    // Generate list of sequences and insert motif in controlled randomized indeces.
    for (int i = 0; i < NUMBER_OF_SEQUENCES; i++){
      int randomIndex = random.nextInt(SEQUENCE_LENGTH - MOTIF_LENGTH + 2);
      sequences.add(rsf.generateSequence(SEQUENCE_LENGTH));
      sequences.get(i).insertMotif(motif, randomIndex);
      insertedIndices[i] = randomIndex;
    }

    System.out.println("Created sequences and real indices are the following: ");
    for (int i = 0; i < insertedIndices.length; i++){
      System.out.print(insertedIndices[i] + " ");
    }
    System.out.println();

    Long GIBBS_SEED = new Long(Long.parseLong(args[2]));
   Profile p = new Profile(sequences, MOTIF_LENGTH);
   p.buildProfile(insertedIndices, 1);
    System.out.println(p.toString(p.getMatrix())); 
   //p.buildAlignmentMatrix(insertedIndices);
   System.out.println(p.toString(p.getAlignmentMatrix())); 
   //p.buildDiscreteProfile(1);
   System.out.println(p.toString(p.getDiscreteProfile())); 
   //p.buildProbabilityProfile(1);
   System.out.println(p.toString(p.getP())); 
   p.buildP_GS(1);
    double sum = 0.0;
    for ( double d : p.getP_GS() ) {
        sum+=d;
    }
    System.out.println("Sum of P_GS array: " + sum);
   System.out.println((Arrays.toString(p.getP_GS()))); 
  }
}
