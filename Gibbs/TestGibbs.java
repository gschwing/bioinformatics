import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
public class TestGibbs {
  // Arguments must be a location seed and a factory seed.
  public static void main(String[] args){

    if ( args.length != 4 ) { 
        System.out.println("\n\nUsage: \targ1 - Seed for Random object\n \targ2 - Seed for RandomSeqFactory\n\targ3 - Seed for Gibbs Sampler\n\targ4 - Number of times to run gibbs, since it won't always reach the optimal answer\n\n");
        System.exit(0);
    }
    final int NUMBER_OF_SEQUENCES = 200;
    final int MOTIF_LENGTH = 15;
    final int SEQUENCE_LENGTH = 900;
    
    Random random = new Random();
    random.setSeed(Long.parseLong(args[0]));

    RandomSequenceFactory rsf = new RandomSequenceFactory(new DNASymbolSet(), Long.parseLong(args[1]));
    Motif motif = new Motif("ACTGTTGCATGATCC", new DNASymbolSet());
    ArrayList<Sequence> sequences = new ArrayList<Sequence>();
    int[] insertedIndices = new int[NUMBER_OF_SEQUENCES];
    // Generate list of sequences and insert motif in controlled randomized indeces.
    for (int i = 0; i < NUMBER_OF_SEQUENCES; i++){
      int randomIndex = random.nextInt(SEQUENCE_LENGTH - MOTIF_LENGTH);
      sequences.add(rsf.generateSequence(SEQUENCE_LENGTH));
      sequences.get(i).insertMotif(motif, randomIndex);
      insertedIndices[i] = randomIndex;
    }

    System.out.println("Created sequences and real indices are the following: ");
    for (int i = 0; i < insertedIndices.length; i++){
      System.out.print(insertedIndices[i] + " ");
    }
    System.out.println();

    Long GIBBS_SEED = new Long(Long.parseLong(args[2]));
    Gibbs g = new Gibbs(sequences, MOTIF_LENGTH);
    int[] gibbsIndices = new int[NUMBER_OF_SEQUENCES];
    int[] bar = new int[NUMBER_OF_SEQUENCES];
    gibbsIndices = g.gibbsSampling(GIBBS_SEED);
    Profile p = new Profile(sequences, MOTIF_LENGTH);
    int hammingDistance = 10000;
    int foo;
    long q = 0;
    long winSeed = 0;
    long arg3 = Long.parseLong(args[3]);
    for ( long i = 0; i < arg3; i++ ) {
        bar = g.gibbsSampling(q = GIBBS_SEED + i);
        p.buildProfile(bar);
        if (( foo = p.hammingDistance()) < hammingDistance ){
            gibbsIndices = bar;
            hammingDistance = foo;
            winSeed = q;   
            if (hammingDistance == 0) 
                break;
        }
    }
    System.out.println("Gibbs indices: " + Arrays.toString(gibbsIndices)); 
    System.out.println("Hamming Score: " + hammingDistance);
    System.out.println("Seed: " + winSeed);
    }
}
