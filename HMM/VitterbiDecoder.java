import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
public class VitterbiDecoder {

    private HMM       model;
    private SymbolSet symset = null;
    private boolean verbose;

    public VitterbiDecoder(HMM model, boolean verbose) {
        this.verbose = verbose;
        this.model = model;
        int modelSize= model.getNumStates();
        if (modelSize > 2)
            symset = model.getState(1).getSymbolSet();
    }

    public ArrayList<HMMState> decodeLogSpace(ArrayList<Character> symbols) {
        ArrayList<HMMState> mostProbableStates = new ArrayList<HMMState>();
        DPM dpm = new DPM(model, symbols);
        dpm.buildDPM();

        if (verbose) {
            System.out.print(dpm);
            System.out.print(dpm.getPath());
        }
        return decodeStatesFromMatrix(dpm.getUnderlyingArray(), dpm.getPath().getUnderlyingArray(), dpm.getRows());
    }

   private ArrayList<HMMState> decodeStatesFromMatrix(double[][] dpm, Coord[][] path, ArrayList<StateEmit> statePerRow){
      ArrayList<HMMState> statePath = new ArrayList<HMMState>();

      // Get the most likely last state's index.
      double maxValue = dpm[0][dpm[0].length - 1];
      int maxIndex = 0;
      for (int i = 1; i < dpm.length; i++){
        if (dpm[i][dpm[0].length - 1] > maxValue){
          maxValue = dpm[i][dpm[0].length - 1];
          maxIndex = i;
        }
      }

      // Coord class has row and col of the path packaged into a nice class
      // we are retrieving the coords of the max likely end state
      Coord finalCoord = path[maxIndex][dpm[0].length - 1];

      // Add the last state
      statePath.add( statePerRow.get(maxIndex).getState() );
      int row;

      // iterate backwards through the path always getting the previous col coords and state
      for ( int i = finalCoord.getCol(); !finalCoord.equals(0,0); i = finalCoord.getCol() ) {
           finalCoord = path[row = finalCoord.getRow()][i];
           statePath.add( statePerRow.get(row).getState() );
      }

      HMMState start = new HMMState(new NullSymbolSet(), new Random());
      start.setName("start");
      start.setShortName('S');

      // Add the start state to the end
      statePath.add(start);

      // Reverse the states
      Collections.reverse(statePath);

      // Return pi
      return statePath;
    }
}
