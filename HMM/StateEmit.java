public class StateEmit {
    private HMMState   state;
    private Character  symbol;

    public StateEmit(HMMState state, Character symbol) {
        this.state = state;
        this.symbol = symbol;
    }

    public HMMState getState() {
        return state;
    }

    public Character getSymbol() {
        return symbol;
    }

    public String toString() {
        String val = "";
        val += state.getName() + ":" + symbol;
        return val;
    }
}
