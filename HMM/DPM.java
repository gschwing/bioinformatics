import java.util.*;
//
public class DPM {

    private HMM model;
    private ArrayList<StateEmit> rows;    // States that know their probabilities
    private ArrayList<Character> symbols; // columns
    private int numRows;
    private int numCols;
    private double[][] dpm;
    private Path path;

    public DPM(){}

    public DPM( HMM model, ArrayList<Character> symbols ) {
        this.model = model;
        this.rows = new ArrayList<StateEmit>();
        this.symbols = symbols;
        populateRows(); // Populates the Row ArrayList with the model's combinations of { State | Emit }
        this.numRows = rows.size();
        this.numCols = symbols.size();
        dpm = new double[this.numRows][this.numCols];
        path = new Path(this.numRows, this.numCols, this);
    }


    public void buildDPM(){

        double emissionProb = 0.0;
        double transitionProb = 0.0;
        for (int column=0; column < numCols; column++) {
            for (int row = 0; row < numRows; row++) {
                if (column == 0) {        // we are in "start"
                    transitionProb = Math.log(1.0);
                    emissionProb =  Math.log(1.0);
                    dpm[row][column] = transitionProb + emissionProb;
                    path.recordPath(row, column, -1, -1);
                // it's possible to come from any state in the previous column
                } else {
                    int priorColumn = column - 1;
                    int priorRow = 0;  // need to check that this is a valid thing to do

                    double maxPathProb = Math.log(0);
                    for(int i = 0; i < numRows; i++) {
                        HMMState possiblePriorState;

                        if (column == 1)
                           possiblePriorState = model.getStartState();
                        else
                           possiblePriorState = rows.get(i).getState();

                        HMMState stateNow = rows.get(row).getState();
                        Character symbolNow = symbols.get(column);
                        transitionProb = Math.log(possiblePriorState.getTransitionProbability(stateNow));
                        //System.out.println("Transmission Prob: " + transitionProb);

                        // Negative infinity will return 0 on Math.exp()
                        double pathProb = Double.NEGATIVE_INFINITY;

                        if (symbolNow.equals(rows.get(row).getSymbol())) {
                            emissionProb = Math.log(stateNow.getEmissionProbability(symbolNow));
                            pathProb = dpm[i][priorColumn] + transitionProb + emissionProb;
                        } else {
                            emissionProb = 0.0;
                        }

                        //System.out.println("Checking pathProb " + pathProb + " is double negative infinity");
                        if (pathProb == Double.NEGATIVE_INFINITY) {
                          continue;
                        } else if (pathProb > maxPathProb) {
                          maxPathProb = pathProb;
                          priorRow = i;
                        }
                    }

                    path.recordPath(row, column, priorRow, priorColumn);
                    dpm[row][column] = maxPathProb;
                }

            }
        }
    }

    public double[][] getUnderlyingArray(){
        return this.dpm;
    }

    public String toString(){
        String str = new String("");
        str += "\n\n\t\t\t\t\t\t\t\t\t\t";
        str += "DPM\n\n";
        str += "             ";
        for (int j=0; j<numCols; j++) {
            str += symbols.get(j) + "\t\t";
        }
        str += "\n";
        for (int i=0; i<numRows; i++) {
            str += rows.get(i);
            for (int j=0; j<numCols; j++) {
               str += String.format(" %.12f ", dpm[i][j]);
            }
            str += "\n";

        }
        return str;
    }

    public Path getPath() {
        return this.path;
    }

    private void populateRows(){

        for (int i = 0; i < model.getNumStates(); i++) {
            if (model.getState(i).getNumPossibleEmissions() > 1) {
                numRows += model.getState(i).getNumPossibleEmissions();
                for (int j=0; j<model.getState(i).getNumPossibleEmissions(); j++) {
                    rows.add(new StateEmit(model.getState(i), model.getState(i).getEmission(j)));
                }
            }
        }
    }

    public ArrayList<StateEmit> getRows(){
        return this.rows;
    }

    public ArrayList<Character> getCols(){
        return this.symbols;
    }
}
