public class run {
    public static void main (String [] args) {
        if ( args.length != 3 ) {
            System.out.println();
            System.out.println("Usage: arg0 - \t\t\"-s\" show programming matrix and path\n\t\t\t\"-h\" do not show");
            System.out.println();
            System.out.println("Usage: arg1 - \t\tnumber of times coin is flipped");
            System.out.println();
            System.out.println("Usage: arg2 - \t\t\"-d\" run as dice symbol set");
            System.out.println();
            System.exit(0);
        }
        if ( !(args[0].equals("-s") || args[0].equals("-h") ) ) {
            System.out.println();
            System.out.println("Usage: arg0 - \t\t\"-s\" show programming matrix and path\n\t\t\t\"-h\" do not show");
            System.out.println();
            System.exit(0);
        }
        else if ( !isNumeric(args[1]) ) {
            System.out.println();
            System.out.println("Usage: arg1 - \t\tnumber of times coin is flipped");
            System.out.println();
            System.exit(0);
        }

        if (args[2].equals("-d")){
          DiceTester.main(args);
        } else {
          VitterbiTester.main( args );
        }
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
