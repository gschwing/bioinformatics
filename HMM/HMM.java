import java.util.Random;
import java.util.ArrayList;

public class HMM {

    private ArrayList<HMMState> myStates;
    private HMMState            startState;
    private HMMState            endState;
    private HMMState            currentState;
    private Random              ran;

    // for use in generative runs;
    private ArrayList<Character>  emittedSymbols = null;
    private ArrayList<HMMState>   statePath = null;
    

    public HMM( ArrayList<HMMState> myStates,
                HMMState            startState,
                Random              ran) {

        this.myStates = myStates;
        this.startState = startState;
        this.currentState = startState;
        this.ran = ran;
    }

    public void generate(int numTransitions) {

        if (emittedSymbols == null)
            emittedSymbols = new ArrayList<Character>();

        if (statePath == null)
            statePath = new ArrayList<HMMState>();

        for (int i=0; i<numTransitions; i++) {
           Character s = currentState.emitSymbol();
           emittedSymbols.add(s);
           statePath.add(currentState);
           currentState = currentState.transition();
        }

    }

    public ArrayList<Character> getEmissions() {
        return emittedSymbols;
    }
    
    public ArrayList<HMMState> getStates() {
        return statePath;
    }

    public HMMState getState(int index) {
        return myStates.get(index);
    }    

    public HMMState getStartState() {
        return startState;
    }

    public int getNumStates() {
        return myStates.size();
    
    }    

    public void reset() {
        emittedSymbols = new ArrayList<Character>();
        statePath = new ArrayList<HMMState>();
        currentState = startState;
    }

}
