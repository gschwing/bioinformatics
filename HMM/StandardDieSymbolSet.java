public class StandardDieSymbolSet extends SymbolSet {

    public StandardDieSymbolSet() {
        super(new char[] {'1','2','3','4','5','6'});
    }

}
