public class NullSymbolSet extends SymbolSet {

    public NullSymbolSet() {
        super(new char[0]);
    }

}
