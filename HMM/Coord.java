
public class Coord{
    private int row;
    private int col;
    public Coord(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() { return this.row;}
    public int getCol() { return this.col;}

    public String toString() {
        return this.row + " , " + this.col;
    }
    
    public boolean equals(int i, int j) {

        if ( i == this.row  && j == this.col)
            return true;
        else 
            return false;
    
    }
}
