public class DNASymbolSet extends SymbolSet {

    public DNASymbolSet() {
        super(new char[] {'A','C','T','G'});
    }

}
