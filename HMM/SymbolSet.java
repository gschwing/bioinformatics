import java.util.*;

public abstract class SymbolSet {

    protected ArrayList<Character> symbols;

    public SymbolSet(char[] characters) {
		symbols = new ArrayList<Character>();
		for (int i=0; i<characters.length; i++)
            symbols.add(characters[i]);
    }

    public int size() {
        return symbols.size();
    }

    public Character get(int i) {
        return symbols.get(i);
    }

    public int getIndex(Character c) {
        return symbols.indexOf(c);
    }

    public boolean contains(Character c) {
        return symbols.contains(c);
    }

}
