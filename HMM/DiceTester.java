import java.util.Random;
import java.util.ArrayList;

public class DiceTester {
    private boolean verbose;
    public static void main(String[] args) {

        Random ran = new Random();

        HMMState start = new HMMState(new NullSymbolSet(), ran);
        start.setName("start");
        start.setShortName('S');

        HMMState fair = new HMMState(new DiceSymbolSet(), ran);
        fair.setName("fair");
        fair.setShortName('F');

        HMMState load = new HMMState(new DiceSymbolSet(), ran);
        load.setName("load");
        load.setShortName('L');


        // now that states are built, set up transitions
        start.addTransition(fair, 0.50);
        start.addTransition(load, 0.50);

        fair.addTransition(fair, 0.90);
        fair.addTransition(load, 0.10);

        load.addTransition(load, 0.90);
        load.addTransition(fair,0.10);


        fair.setEmissionProbability(0,0.166666);
        fair.setEmissionProbability(1,0.166666);
        fair.setEmissionProbability(2,0.166667);
        fair.setEmissionProbability(3,0.166667);
        fair.setEmissionProbability(4,0.166667);
        fair.setEmissionProbability(5,0.166667);

        load.setEmissionProbability(0,0.05);
        load.setEmissionProbability(1,0.28);
        load.setEmissionProbability(2,0.05);
        load.setEmissionProbability(3,0.28);
        load.setEmissionProbability(4,0.05);
        load.setEmissionProbability(5,0.29);


        if (args[0].equals("-s")) {
            System.out.println("Fair sane? : " + fair.isSane());
            System.out.println("Loaded sane? : " + load.isSane());
            System.out.println("Start sane? : " + start.isSane());
        }

        ArrayList<HMMState> states = new ArrayList<HMMState>();
        states.add(load);
        states.add(fair);

        HMM myModel = new HMM(states,start,ran);

        int numtries = Integer.parseInt(args[1]);

        myModel.generate(numtries);

        ArrayList<Character> signal = myModel.getEmissions();
        ArrayList<HMMState> actualpi = myModel.getStates();

        if (args[0].equals("-s")) {
            for (int i=0; i<numtries; i++) {
                System.out.println("State: " + actualpi.get(i) + "  Emits: " + signal.get(i));
            }
        }

        VitterbiDecoder myDecoder;

        if (args[0].equals("-s")) {
            myDecoder = new VitterbiDecoder(myModel, true);
        } else {
            myDecoder = new VitterbiDecoder(myModel, false);
        }

        ArrayList<HMMState> decodedpi = myDecoder.decodeLogSpace(signal);


        System.out.println("\n");
        System.out.println("Decoded Pi : ");

       for (int i=0; i<decodedpi.size(); i++) {
            System.out.print(decodedpi.get(i) + " , ");
        }

        System.out.println("\n");
        System.out.println("\nActual pi : ");

        for (int i = 0; i < actualpi.size(); i++){
            System.out.print(actualpi.get(i) + " , ");
        }

        System.out.println("\n");
        System.out.println("\n");

        double score = 0.0;

        for ( int i = 1; i < actualpi.size(); i++ ){
            if ( actualpi.get(i).equals(decodedpi.get(i)) )
                score++;
        }

        score = ( score / ( actualpi.size() - 1 )) * 100;
        System.out.println("Accuracy Score: " + score);


    }


}
