import java.util.ArrayList;
import java.util.Random;

public class HMMState {

    private SymbolSet           mySymbolSet;
    private ArrayList<HMMState> linkages;
    private ArrayList<Double>   transitionProbabilities;
    private Double[]            emissionProbabilities;
    private Random              ran;
    private String              stateName;
    private char                shortStateName;


    public HMMState(SymbolSet s, Random ran) {
        mySymbolSet = s;
        linkages = new ArrayList<HMMState>();
        transitionProbabilities = new ArrayList<Double>();
        emissionProbabilities = new Double[mySymbolSet.size()];
        this.ran = ran;
        this.stateName = "";
        this.shortStateName = 'N';
    }

    public void setEmissionProbability(int index, Double prob) {
        emissionProbabilities[index] = prob;
    }

    public void addTransition(HMMState otherstate, Double prob) {
        linkages.add(otherstate);
        transitionProbabilities.add(prob);
    }    

    public Character emitSymbol() {

        double ranVal = ran.nextDouble();
        double sum = 0.0;
        Character returnVal = null;
        for (int i=0; i<emissionProbabilities.length; i++) {
            sum += emissionProbabilities[i];
            if (ranVal <= sum ) {
                returnVal =  mySymbolSet.get(i);
                break;
            }
        } 
        return returnVal;
    }

    public HMMState transition() {

        double ranVal = ran.nextDouble();
        double sum = 0.0;
        HMMState returnVal = null;
        for (int i=0; i<transitionProbabilities.size(); i++) {
            sum += transitionProbabilities.get(i);
            if (ranVal <= sum ) {
                returnVal =  linkages.get(i);
                break;
            }
        } 
        return returnVal;
    }

    public void setName(String s) {
        stateName = s;
    }

    public void setShortName(char c) {
        shortStateName = c;
    }

    public String getName() {
        return stateName;
    }

    public SymbolSet getSymbolSet() {
        return mySymbolSet;
    }

    public Character getShortName() {
        return shortStateName;
    }

    public boolean isSane() {

        double tolerance = 0.01;

        double sumTrans = 0.0;
        for (double val : transitionProbabilities)
            sumTrans += val;

        double sumEmit = 0.0;
        for (double val : emissionProbabilities)
            sumEmit += val;


        if ( (Math.abs(sumTrans - 1.0) < tolerance) && 
             (  (Math.abs(sumEmit - 1.0) < tolerance) ||  // normal emission profile
                (Math.abs(sumEmit) < tolerance) )       ) // no emission
            return true;

        return false;

    }

    public int getNumPossibleTransitions() {
        return linkages.size();
    }
    
    public HMMState getTransition(int i) {
        return linkages.get(i);
    }

    public double   getTransitionProbability(int i) {
        return transitionProbabilities.get(i);
    }

    public double   getTransitionProbability(HMMState s) {
        double val = 0.0;
        for (int i=0; i<linkages.size(); i++)  {
            if ( linkages.get(i) == s) {
                val = transitionProbabilities.get(i);
                break;
            }
        }
        return val;
    }

    public int getNumPossibleEmissions() {
        return emissionProbabilities.length;
    }

    public Character getEmission(int i) {
        return mySymbolSet.get(i);
    }

    public double getEmissionProbability(int i) {
        return emissionProbabilities[i];
    }

    public double getEmissionProbability(Character c) {
        double  val = 0.0;
        for (int i=0; i<emissionProbabilities.length; i++) {
            if (c ==  mySymbolSet.get(i)) {
                val = emissionProbabilities[i];
                break;
            }
        }
        return val;
    }    

    public String toString() {
        String rv = "";
        rv += getShortName();
        return rv;
    }

}
