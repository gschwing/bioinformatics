import java.util.Random;
import java.util.ArrayList;

public class HMMTester2 {

    public static void main(String[] args) {
    
        Random ran = new Random();

        HMMState start = new HMMState(new NullSymbolSet(), ran);
        start.setName("start");
        start.setShortName('S');

        HMMState fair = new HMMState(new CoinSymbolSet(), ran);
        fair.setName("fair");
        fair.setShortName('F');

        HMMState loaded = new HMMState(new CoinSymbolSet(), ran);
        loaded.setName("loaded");
        loaded.setShortName('L');


        // now that states are built, set up transitions
        start.addTransition(fair, 0.50);
        start.addTransition(loaded, 0.50);

        fair.addTransition(fair, 0.90);
        fair.addTransition(loaded, 0.10);

        loaded.addTransition(loaded, 0.90);
        loaded.addTransition(fair,0.10);


        fair.setEmissionProbability(0,0.50);
        fair.setEmissionProbability(1,0.50);
    
        loaded.setEmissionProbability(0,0.75);
        loaded.setEmissionProbability(1,0.25);


        System.out.println("Fair sane? : " + fair.isSane());
        System.out.println("Loaded sane? : " + loaded.isSane());
        System.out.println("Start sane? : " + start.isSane());

        ArrayList<HMMState> states = new ArrayList<HMMState>();
        states.add(loaded);
        states.add(fair);
        states.add(start);

        HMM myModel = new HMM(states,start,ran);

        int numtries = 50;

        myModel.generate(numtries);

        ArrayList<Character> signal = myModel.getEmissions();
        ArrayList<HMMState> pi = myModel.getStates();

        for (int i=0; i<numtries; i++) {
            System.out.println("State: " + pi.get(i) + "  Emits: " + signal.get(i));
        }

    }


}
