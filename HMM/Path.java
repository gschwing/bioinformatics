import java.util.*;

public class Path {
    private Coord[][] path;
    private DPM dpm; 
    public Path(int i, int j, DPM dpm) {
        this.path = new Coord[i][j];
        this.dpm = dpm;
    }

    public void recordPath( int i, int j, int priorRow, int priorCol){
        path[i][j] = new Coord(priorRow, priorCol);
    }   
    
    public Coord[][] getUnderlyingArray(){
        return this.path;
    }

    public String toString() {
        String str = new String("");
        ArrayList<Character> cols = dpm.getCols();
        ArrayList<StateEmit> rows = dpm.getRows();
        str += "\n\n\t\t\t\t\t\t\t\t\t\t";
        str += "Path\n";
        str += "\n\n";
        str += "             ";
        for (int i = 0; i < cols.size(); i++)
            str += cols.get(i) + "\t\t";
        str += "\n";
        for (int i = 0; i < path.length; i++) {
            str += rows.get(i) + "\t   ";
            for (int j = 0; j < path[i].length; j++) {
                str += path[i][j] + "\t     ";
          }
          str += "\n";
        }
    return str;    
    }
    
}

