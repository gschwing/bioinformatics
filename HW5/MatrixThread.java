public class MatrixThread extends Thread implements Runnable {

    int thread_id;
    int numElements;
    static double[][] matrix1;
    static double[][] matrix2;
    static double[][] matrix3;
    boolean lastThread;
    
    public MatrixThread ( int thread_id, 
                    int numElements,
                    double[][] matrix1,
                    double[][] matrix2,
                    double[][] matrix3,
                    boolean lastThread) {

        this.thread_id = thread_id;
        this.numElements = numElements;
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.matrix3 = matrix3;
        this.lastThread = lastThread;

    }

    @Override
    public void run(){
        int offset = thread_id*numElements;
        int rowNum = 0;
        int colNum = 0;
        System.out.println("Ran " + thread_id);
        if (lastThread){
            for ( int i = 0 + offset; i < 10000; i++) {

                rowNum = i / 100;
                colNum = i % 100;
                double temp = 0.0;

                for (int z = 0; z < 100; z++) {

                    temp += matrix1[rowNum][z]*matrix2[z][colNum];

                }

                matrix3[rowNum][colNum] = temp;                     

            }

        } else {
    
            for ( int i = 0 + offset; i < offset + numElements; i++) {
                
                rowNum = i / 99;
                colNum = i % 99;
                double temp = 0.0;

                for (int z = 0; z < 100; z++) {

                    temp += matrix1[rowNum][z]*matrix2[z][colNum];

                }

                matrix3[rowNum][colNum] = temp;                     

            }
        }
    }
}        
