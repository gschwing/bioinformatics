import java.util.Random;
import java.util.concurrent.*;
import java.io.*;

public class MatMul_thread {


    public static void main ( String [] argsv ) throws InterruptedException, IOException {

    int numThreads = Integer.parseInt(argsv[0]);

    double [][] matrix1 = new double[100][100];
    double [][] matrix2 = new double[100][100];
    double [][] matrix3 = new double[100][100];

    // Give the same seed every time so no run randomly has "more difficult" numbers
    Random r = new Random(1234);
    double sign = 1.0;

        for ( int i = 0; i < matrix1.length; i++ ) {
            for ( int j = 0; j < matrix1[i].length; j++ ) {

                sign = 1.0;

                if ( r.nextInt() <= r.nextInt() )
                    sign = -1.0;
                                    
                matrix1[i][j] = r.nextDouble() * 100.0 * sign;
            
            }
        }
        
        for ( int i = 0; i < matrix2.length; i++ ) {
            for ( int j = 0; j < matrix2[i].length; j++ ) {

                sign = 1.0;

                if ( r.nextInt() <= r.nextInt() )
                    sign = -1.0;
                                    
                matrix2[i][j] = r.nextDouble() * 100.0 * sign;
            
            }
        }

        int elementsPerThread = 0;
        
        elementsPerThread = 10000/numThreads;

        boolean lastThread = false;

        ExecutorService es = Executors.newCachedThreadPool();
        for ( int k = 0; k < numThreads; k++ ) {
           
            if ( k + 1 == numThreads )
                lastThread = true;
 
            es.execute(new MatrixThread(k, elementsPerThread, matrix1, matrix2, matrix3, lastThread));
        }
        es.shutdown();
        while(!es.awaitTermination(1, TimeUnit.MINUTES));
        // courtesy of https://stackoverflow.com/questions/7939257/wait-until-all-threads-finish-their-work-in-java/7939272?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

        String fileName = numThreads + "_threads.csv";

        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        
        String str = "";

        int rowNum = 0;
        int colNum = 0;
        for ( int i = 0; i < 100; i++) {
            for ( int j = 0; j < 100; j++ ) {

             str += matrix3[i][j] + ","; 

            }

            str+= "\n";
        }

        writer.write(str);
 
        writer.close(); 

    } 
}         
