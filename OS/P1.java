import java.util.concurrent.*;
import java.util.*;
import java.util.Arrays;
import java.util.ArrayList;

public class P1 {

    
    public static void main(String[] args) {
        Pizza A = null;
        Pizza B = null;
        Pizza C = null;

	ExecutorService executor = Executors.newCachedThreadPool();
            
        A = new Pizza("A") {
                    public void run() {
                        enter_oven();
                    }
        };
        executor.submit(A);
       
	
        B = new Pizza("B") {
                    public void run() {
                        enter_oven();
                    }
        };
        executor.submit(B);

        C = new Pizza("C") {
                    public void run() {
                        enter_oven();
                    }
        };
        executor.submit(C);


 
        try {
        Thread.sleep(6000);
        } catch (InterruptedException e) {}
        
    }
}
