import java.util.concurrent.*;
import java.util.*;
import java.util.Arrays;
import java.util.ArrayList;

public class PerfectNumber {

    
    public static void main(String[] args) {
        int p;
        int N;
        int numbers_per_thread;
        int return_array_size;
        PerfectNumCalc r = null;

        p = Integer.parseInt(args[0]);
        System.out.printf("Parsed first arg , num threads : %d \n", p);
        N = Integer.parseInt(args[1]);
        System.out.printf("Parsed second arg , input number : %d \n", N);
	    numbers_per_thread = ( (N+p-1) / p);	
        System.out.println("N per T : " + numbers_per_thread); 
 
        int[] inArray = new int[numbers_per_thread];
        
		ExecutorService executor = Executors.newCachedThreadPool();
            
        boolean Ready_To_Pass = false;

		for ( int i = 0; i < N; i++ ) {

            if (!Ready_To_Pass){
                System.out.println("Not ready to pass");
                inArray[ i % numbers_per_thread ] = i+1;
                System.out.println("i : " + i + " i % NPT : " + i % numbers_per_thread);
            }
            
            if ( i % numbers_per_thread == inArray.length - 1){
                System.out.println("Ready to pass");
                System.out.println("i : " + i + " i % NPT : " + i % numbers_per_thread + " inArray.length - 1 : " + (inArray.length - 1));
                Ready_To_Pass = true;
            }


            if (Ready_To_Pass) { 
                System.out.println("The factors, "+ Arrays.toString(inArray));
                r = new PerfectNumCalc(N, inArray) {
                    public void run() {
                        PerfectNum();
                    }
                };
        	    executor.submit(r);
                Ready_To_Pass = false;
            }   //System.out.println("Line 31 - runner");
        }
        //this line will execute immediately, not waiting for your task to complete
		int temp = 0;
        
        try {
        Thread.sleep(6000);
        } catch (InterruptedException e) {}
        
        ArrayList<Integer> factors = PerfectNumCalc.getFactorArrayList(); 

		for (int i = 0; i < factors.size(); i++){
				temp += factors.get(i);
		}
		String evalString = "";

		if (temp == N)
			evalString = "";
		else
			evalString = "NOT";

		System.out.printf("The factors, "+ Arrays.toString(factors.toArray()) + " sum to " + temp + " which is " + evalString + " a perfect number", temp, evalString);
    }
}
