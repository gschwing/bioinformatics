import java.util.ArrayList;

public class Pizza extends Thread implements Runnable{

    static int count = 0; // shared counter to prove semaphores work
    static int MUTEX = 1; // shared counter to prove semaphores work
	String name;
	
	public Pizza(String name){
		this.name = name;
		count++;
	}

	public void run(){
		if (count == 3) 
		enter_oven();
	}

	public void try_to_cook(){
        	down();
		cook();
		up();
	}

	public void down() {
		System.out.println("Pizza " + name + "trying to enter oven.");
		if (MUTEX == 0)
			return;
		else {
			Thread.yield();
			down();
		}
	}

	public void up() {
		MUTEX = 1;
		return;
	}
	
	public void cook(){
		System.out.println("Pizza " + name + " is cooked");
	}
}

