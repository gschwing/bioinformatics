public class TestMotif {
  public static void main(String[] args){
    Motif motif = new Motif("ACTGACACAGTC" , new DNASymbolSet() );
    System.out.println("Motif: " + motif);

    RandomSequenceFactory rsf = new RandomSequenceFactory( new DNASymbolSet() );
    Sequence sequence = rsf.generateSequence(40);
    System.out.println("Sequence: " + sequence);

    sequence.insertMotif(motif, 3);
    System.out.println("Seq Motif: " + sequence);

    System.out.println("Mutation 1: " + motif.mutateMotif(0.05));
    System.out.println("Mutation 2: " + motif.mutateMotif(0.005));
  }
}
