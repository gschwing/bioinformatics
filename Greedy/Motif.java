import java.util.Random;
public class Motif extends Sequence {

  private String sequenceString;
  private SymbolSet symbolSet;
  private Random random = new Random();

  public Motif (String sequenceString, SymbolSet symbolSet){

    super(symbolSet);
    this.sequenceString = sequenceString;
    this.symbolSet = symbolSet;

    for (int i = 0; i < sequenceString.length(); i++){
      append(sequenceString.charAt(i));
    }
  }

  public Motif mutateMotif(double probability){
    Motif motif = new Motif(this.sequenceString, this.symbolSet);
    int counter = 0;
    for (int i = 0; i < motif.length(); i++){
      if (random.nextDouble() < probability){
        counter++;
        motif.sequence.set(i, symbolSet.get(random.nextInt(symbolSet.size())));
      }
    }
    System.out.println("Mutated " + counter + " times ");
    System.out.println(this);
    return motif;

  }
}
