import java.util.*;
public class GreedyMotifSearch{

  private ArrayList<Sequence> sequences;
  private int amountOfSequences;
  private int motifLength;
  private Random random;



  public GreedyMotifSearch(ArrayList<Sequence> sequences, int motifLength, int seed){
    this.sequences = sequences;
    this.motifLength = motifLength;
    random = new Random();
    random.setSeed(seed);
  }

  public Profile getBestProfile(){
    // Randomly generate a set of integers as starting indices.
    int[] priorStartingIndices = new int[sequences.size()];
    for (int i = 0; i < priorStartingIndices.length; i++){
      priorStartingIndices[i] = random.nextInt(sequences.get(i).length() - motifLength + 1); // Random index from 0 to n - l + 1
    }
    // Create a profile from sequences and set of indices.
    Profile profile = new Profile(sequences, priorStartingIndices, motifLength);
    // Create a bestScore which is 0, yes however ... 
    int bestHamming = profile.totalHammingDistance(); // This won't be zero?  
    System.out.println("Starting hamming distance is " + bestHamming);
    System.out.println("Starting indices are: ");
    for (int i =0; i < priorStartingIndices.length; i++){
      System.out.print(priorStartingIndices[i] + " ");
    }
    System.out.println();
    // while the score of the profile from the sequence (hamming distance is less than current)
    Scanner input =  new Scanner(System.in);
    int[] newStartingIndices = new int[priorStartingIndices.length];
    boolean hasConverged = false;

    while (!hasConverged){

      // iterate through each sequence
      for (int i = 0; i < sequences.size(); i++){
        newStartingIndices[i] = profile.getIndexOfMostLikely(sequences.get(i));
      }

      //for (int i =0; i < startingIndices.length; i++){
      //  System.out.print(startingIndices[i] + " ");
      //}
      //System.out.println();

      for (int i =0; i < newStartingIndices.length; i++){
        System.out.print(newStartingIndices[i] + " ");
      }
      System.out.println();

      profile = new Profile(sequences, newStartingIndices, motifLength);
      bestHamming = profile.totalHammingDistance();
      //System.out.println("Current best hamming: " + bestHamming);
      if (indicesAreEqual(newStartingIndices, priorStartingIndices)){
        hasConverged = true;
      } else {
        for (int i = 0; i < newStartingIndices.length; i++){
          priorStartingIndices[i] = newStartingIndices[i];
        }
      }


    }

    //System.out.println(profile.toString());
    return profile;
  }

  public int[] getBestIndices(){
    // iterate through each sequence
    // Randomly generate a set of integers as starting indices.
    int[] priorStartingIndices = new int[sequences.size()];
    for (int i = 0; i < priorStartingIndices.length; i++){
      priorStartingIndices[i] = random.nextInt(sequences.get(i).length() - motifLength); // Random index from 0 to length of lmer
    }
    // Create a profile from sequences and set of indices.
    Profile profile = new Profile(sequences, priorStartingIndices, motifLength);
    // Create a bestScore which is 0
    int bestHamming = profile.totalHammingDistance();
    //System.out.println("Starting hamming distance is " + bestHamming);
    //System.out.println("Starting indices are: ");
    // for (int i =0; i < priorStartingIndices.length; i++){
    //   System.out.print(priorStartingIndices[i] + " ");
    // }
    // System.out.println();
    // while the score of the profile from the sequence (hamming distance is less than current)
    Scanner input =  new Scanner(System.in);
    int[] newStartingIndices = new int[priorStartingIndices.length];
    boolean hasConverged = false;

    while (!hasConverged){

      // iterate through each sequence
      for (int i = 0; i < sequences.size(); i++){
        newStartingIndices[i] = profile.getIndexOfMostLikely(sequences.get(i));
      }

      //for (int i =0; i < startingIndices.length; i++){
      //  System.out.print(startingIndices[i] + " ");
      //}
      //System.out.println();

      // for (int i =0; i < newStartingIndices.length; i++){
      //   System.out.print(newStartingIndices[i] + " ");
      // }
      // System.out.println();

      profile = new Profile(sequences, newStartingIndices, motifLength);
      bestHamming = profile.totalHammingDistance();
      //System.out.println("Current best hamming: " + bestHamming);
      if (indicesAreEqual(newStartingIndices, priorStartingIndices)){
        hasConverged = true;
      } else {
        for (int i = 0; i < newStartingIndices.length; i++){
          priorStartingIndices[i] = newStartingIndices[i];
        }
      }


    }

    //System.out.println(profile.toString());
    return newStartingIndices;

  }

  private static boolean indicesAreEqual(int[] array1, int[] array2){

    for (int i = 0; i < array1.length; i++){
      if (array1[i] != array2[i]){
        return false;
      }
    }

    return true;

  }

}
