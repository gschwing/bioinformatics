import java.util.*;

public class TestGreedy{




	public static void main(String[] args){

  Random random = new Random();

	// number of DNA strings
	final int NUMBER_OF_SEQUENCES = 20;
  final int SEQUENCE_LENGTH = 100;
  final int MOTIF_LENGTH = 8;
        // DNA Sequence Factory
	RandomSequenceFactory foo = new RandomSequenceFactory( new DNASymbolSet(), Integer.parseInt(args[0]));

	ArrayList<Sequence> sequences = new ArrayList<Sequence>();
	//Populate Sequences ArrayList
	Sequence sequence = null;
	Motif motif = new Motif("TCGATCGA", new DNASymbolSet() );
  int[] insertionIndices = new int[NUMBER_OF_SEQUENCES];

	for(int i = 0; i < NUMBER_OF_SEQUENCES; i ++){
    int randomIndex = random.nextInt(SEQUENCE_LENGTH - MOTIF_LENGTH);
    insertionIndices[i] = randomIndex;
		sequence = foo.generateSequence(SEQUENCE_LENGTH);
		sequence.insertMotif(motif, randomIndex);
		sequences.add(i, sequence);
	}
  
  System.out.println("Motifs inserted at indices: ");
  for (int i = 0; i < NUMBER_OF_SEQUENCES; i++){
    System.out.print(insertionIndices[i] + " ");
  }
  System.out.println();
	GreedyMotifSearch gms = new GreedyMotifSearch(sequences, motif.length(), Integer.parseInt(args[1]));

	Profile profile = gms.getBestProfile();
	System.out.println(profile);
	System.out.println("Hamming Distance: "+profile.totalHammingDistance());
	System.out.println("\nStart Indices of motif: " + Arrays.toString(profile.getStartIndices()));
  System.out.println("Motif: " + profile.mostProbable());
	System.out.print("\n\n");
	}
}
