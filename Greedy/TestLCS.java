public class TestLCS {
  public static void main(String[] args){

    Sequence v = new Sequence(new DNASymbolSet(), "TTTTTTATATAAATA");
    Sequence w = new Sequence(new DNASymbolSet(), "GGGGGTATATAAGGGGGCC");
    // Outcome should be:
    // TATATAA


    LongestCommonSequence lcs = new LongestCommonSequence(v, w);
    System.out.println(lcs.getLCSSequence());
    //System.out.println(result);

  }
}
