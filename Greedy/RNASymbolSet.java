public class RNASymbolSet extends SymbolSet {

    public RNASymbolSet() {
        super(new char[] {'A','C','U','G'});
    }

}
