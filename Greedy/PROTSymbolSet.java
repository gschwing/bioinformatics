public class PROTSymbolSet extends SymbolSet {

    public PROTSymbolSet() {
        super(new char[] {'A',
                          'C',
                          'D',
                          'E',
                          'F',
                          'G',
                          'H',
                          'I',
                          'K',
                          'L',
                          'M',
                          'N',
                          'P',
                          'Q',
                          'R',
                          'S',
                          'T',
                          'V',
                          'W',
                          'Y'});
    }

}
