/***
* Perform multiple iterations of GreedyMotifSearch in a controlled randomized environment
* to see the most commonly converged starting indices and its accuracy.
***/
import java.util.ArrayList;

public class GreedyMotifRunner {

  private static final int SEED = 1000;
  private static final int LOOP_COUNT = 1000;

  private ArrayList<Sequence> sequences;
  private int motifLength;

  public GreedyMotifRunner(ArrayList<Sequence> sequences, int motifLength){
    this.sequences = sequences;
    this.motifLength = motifLength;
  }

  public int[] getMostCommonIndices(){

    ArrayList<int[]> mostCommonIndices = new ArrayList<int[]>(); // Create 1000 slots for each best index array.
    ArrayList<Integer> mostCommonScore = new ArrayList<Integer>();


    GreedyMotifSearch gms = new GreedyMotifSearch(sequences, motifLength, SEED);

    for (int i = 0; i < LOOP_COUNT; i++){
      // For each best index obtained
      int[] currentIndexSet = gms.getBestIndices();
      boolean different = true;

      for (int j = 0; j < mostCommonIndices.size(); j++){
        if (indicesAreEqual(currentIndexSet, mostCommonIndices.get(j))){
          int currentScore = mostCommonScore.get(j);
          mostCommonScore.set(j, currentScore + 1);
          System.out.println("Found a duplicate convergence.");
          different = false;
        }
      }

      if (different){
        mostCommonIndices.add(currentIndexSet);
        mostCommonScore.add(1);
      }
    }

    // Get and return set of indices with the best score.
    int max = 0;
    int maxIndex = 0;
    for (int i = 1; i < mostCommonScore.size(); i++){
      if (mostCommonScore.get(i) > max){
        max = mostCommonScore.get(i);
        maxIndex = i;
      }
    }
    System.out.println(mostCommonIndices.size());
     System.out.println("Most Common scores list:");
     for (int i = 0; i < mostCommonScore.size(); i++){
       System.out.print(mostCommonScore.get(i) + " ");
     }
    return mostCommonIndices.get(maxIndex);
  }

  private static boolean indicesAreEqual(int[] array1, int[] array2){

    for (int i = 0; i < array1.length; i++){
      if (array1[i] != array2[i]){
        //System.out.println(array1[i] + " is not equal to " + array2[i]);
        return false;
      }
    }

    return true;
  }


}
