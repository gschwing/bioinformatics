public class LongestCommonSequence {

  private Sequence v;
  private Sequence w;

  private int n;
  private int m;

  private int[][] matrix;
  private int[][] directionMatrix;

  private static final int UP = 0;
  private static final int LEFT = 1;
  private static final int DIAGONAL = 2;

  public LongestCommonSequence(Sequence v, Sequence w){

    this.v = v;
    this.w = w;

    this.n = v.length();
    this.m = w.length();
    this.calculateLCS();
  }

  private void calculateLCS(){

    this.matrix = new int[n][m];
    this.directionMatrix = new int[n][m];

    for (int i = 0; i < n; i++){
      matrix[i][0] = 0;
    }

    for (int i = 1; i < m; i++){
      matrix[0][i] = 0;
    }

    for (int i = 1; i < n; i++){
      for (int j = 1; j < m; j++){

        if (v.get(i) == w.get(j)){
          matrix[i][j] = max(matrix[i-1][j], matrix[i][j-1], matrix[i-1][j-1] + 1);
        } else {
          matrix[i][j] = max(matrix[i-1][j], matrix[i][j-1]);
        }

        if (matrix[i][j] == matrix[i-1][j]){
          directionMatrix[i][j] = UP;
        } else if (matrix[i][j] == matrix[i][j-1]){
          directionMatrix[i][j] = LEFT;
        } else if (matrix[i][j] == (matrix[i-1][j-1] + 1)){
          directionMatrix[i][j] = DIAGONAL;
        }

      }
    }
  }

  public Sequence getLCSSequence(){
    Sequence LCS = new Sequence( new DNASymbolSet() );
    printDirectionMatrix();
    getLCS(LCS, this.directionMatrix, this.v, n - 1, m - 1);
    return LCS;
  }

  public void getLCS(Sequence LCS, int[][] b, Sequence v, int i, int j){

    if (i == 0 || j == 0){
      return;
    }

    if (b[i][j] == DIAGONAL){
       getLCS(LCS, b, v, i - 1, j - 1);
       LCS.append(v.get(i));
       System.out.println(v.get(i));
    } else {
      if (b[i][j] == UP){
        getLCS(LCS, b, v, i - 1, j);
      } else {
        getLCS(LCS, b, v, i, j - 1);
      }
    }
  }

  private int max(int... vals){
    int max = vals[0];

    for (int i = 1; i < vals.length; i++){
      if (vals[i] > max){
        max = vals[i];
      }
    }

    return max;
  }

  private void printDirectionMatrix(){
    for (int i = 0; i < this.directionMatrix.length; i++){
      for (int j = 0; j < this.directionMatrix[i].length; j++){
        if (this.directionMatrix[i][j] == UP){
          System.out.print("U ");
        } else if (this.directionMatrix[i][j] == LEFT){
          System.out.print("L ");
        } else {
          System.out.print("D ");
        }
      }
      System.out.println();
    }
  }


}
