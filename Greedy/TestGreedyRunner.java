import java.util.Random;
import java.util.ArrayList;

public class TestGreedyRunner {
  // Arguments must be a location seed and a factory seed.
  public static void main(String[] args){

    final int NUMBER_OF_SEQUENCES = 50;
    final int MOTIF_LENGTH = 10;  // Length of our given Motif.
    final int SEQUENCE_LENGTH = 2000; // Length of each sequence.

    Random random = new Random();
    random.setSeed(Integer.parseInt(args[0]));

    RandomSequenceFactory rsf = new RandomSequenceFactory(new DNASymbolSet(), Integer.parseInt(args[1]));
    Motif motif = new Motif("ACCACCATGA", new DNASymbolSet());
    ArrayList<Sequence> sequences = new ArrayList<Sequence>();
    int[] insertedIndices = new int[NUMBER_OF_SEQUENCES];
    // Generate list of sequences and insert motif in controlled randomized indeces.
    for (int i = 0; i < NUMBER_OF_SEQUENCES; i++){
      int randomIndex = random.nextInt(SEQUENCE_LENGTH - MOTIF_LENGTH - 2);
      sequences.add(rsf.generateSequence(SEQUENCE_LENGTH));
      sequences.get(i).insertMotif(motif, randomIndex);
      insertedIndices[i] = randomIndex;
    }

    System.out.println("Created sequences and real indices are the following: ");
    for (int i = 0; i < insertedIndices.length; i++){
      System.out.print(insertedIndices[i] + " ");
    }
    System.out.println();

    // Create Motif Runner instance
    GreedyMotifRunner gmr = new GreedyMotifRunner(sequences, MOTIF_LENGTH);
    int[] obtainedIndices = gmr.getMostCommonIndices(); // Obtained the most commonly converged indices.

    System.out.println("Obtained the following most commonly converged result:");
    for (int i = 0; i < obtainedIndices.length; i++){
      System.out.print(obtainedIndices[i] + " ");
    }
    System.out.println();
    System.out.println();
    System.out.println("This is the profile given the obtained indices and the sequences: ");
    System.out.println(new Profile(sequences, obtainedIndices, MOTIF_LENGTH));
  }
}
