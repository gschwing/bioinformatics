import java.util.*;

public class Profile {

    protected int[] startIndices;
    protected double[][] values;
    protected int[][] totals;
    protected int[] columnTotals;
    protected SymbolSet symbols;
    protected int length;

    public Profile(ArrayList<Sequence> sequences, int[] startIndices, int length) {
        this(sequences,startIndices,length, -1 );
    }

    // overloaded constructor that ignores one of the sequences when building profile
    public Profile(ArrayList<Sequence> sequences, int[] startIndices, int length, int indexOfSeqToIgnore ) {
        this.startIndices = startIndices;
        this.length = length;
        symbols = sequences.get(0).getSymbolSet();

        // Iterate through sequences ArrayList to check for consistent SymbolSet types
        ListIterator<Sequence> litr = sequences.listIterator();
        while(litr.hasNext()) {
            if(!( litr.next().getSymbolSet().equals(symbols))){
               // Data is inconsistent, terminate and print error message
               System.out.println("Element " + litr.nextIndex() + " is of the wrong SymbolSet");
               System.exit(0);
            }
        }

        // first, set up the values array, and a counter array of the same size
        int symbolSetSize = sequences.get(0).getSymbolSet().size();
        this.values = new double[length][symbolSetSize];
        totals = new int[length][symbolSetSize];
        for (int i=0; i<length; i++) {
            for (int j=0; j< symbolSetSize; j++) {
                this.values[i][j] = 0.0d;
                totals[i][j] = 0;
            }
        }

        // Check to see that startIndices length == Sequences length
        if(startIndices.length != sequences.size()){
            System.out.println("Usage Error: Class Profile.java: Number of Sequences must equal number of Start Indices");
            System.exit(1);
        }
        // now, loop over the sequences and accumulate totals of each symbol
        for (int i=0; i<sequences.size(); i++) {    // loop over sequences
            if (i != indexOfSeqToIgnore) { // ignore the one sequence if it has a valid index (for Gibbs Sampling)
                for (int j=0; j<length; j++) {          // loop over symbols in subsequence
                    int indexOfSymbol = symbols.getIndex(sequences.get(i).get(j+startIndices[i]));
                    totals[j][indexOfSymbol]++;
                }  // end loop over subsequence
            }
        }  // end loop of sequences

        // now, finally, loop over the totals matrix to calculate totals per position
        columnTotals = new int[length]; // build and then initialize columnTotalsArray
        for (int i=0; i< length; i++)
            columnTotals[i] = 0;

        for (int i=0; i< length; i++) {
            for (int j=0; j< symbolSetSize; j++) {
               columnTotals[i] += totals[i][j];
            }
        }

        for (int i=0; i< length; i++) {
            for (int j=0; j< symbolSetSize; j++) {
               values[i][j] = (double)totals[i][j] / columnTotals[i];
            }
        }

    }


    public int totalHammingDistance(){
        int totalHammingDistance = 0;
        for( int i = 0; i < this.length; i++){
            int max = 0;
            for( int j = 0; j < this.symbols.size(); j++){
                if (totals[i][j] > max)
                max = totals[i][j];
            }
            totalHammingDistance += columnTotals[i]-max;
        }
        return totalHammingDistance;
    }

    public int[] getStartIndices(){
        return startIndices;
    }

    public String toString() {
        String returnval = "";
        returnval += "\n";
        String title = "Profile Matrix";
        String indent = new String(new char[(this.length*3)-(title.length()/4)]).replace("\0", " ");
        indent += title;
        returnval+= indent;
        returnval += "\n\n";
        for (int i = 0; i < this.length; i++){
            returnval += String.format("      " + i);
        }

        returnval += "\n";
        for (int i = 0; i < this.symbols.size(); i++){
            returnval += symbols.get(i) + "   ";
            for (int j = 0; j < this.length; j++){
                returnval += String.format("%.2f" + "   ", values[j][i]);
            }
            returnval += "\n";
        }
        return returnval;
    }

    // If you pass in a sequence and a starting index,
    // it will return a double that represents the product of the probabilities of the
    // symbols at each column.
    public double getPScore(Sequence sequence, int startingIndex){
      double score = 1;

      for (int i = startingIndex; i < length + startingIndex; i++){
        char current = sequence.get(i);
        score *= values[i - startingIndex][symbols.getIndex(current)];
      }
      return score;
    }

    public int getIndexOfMostLikely(Sequence sequence){
      double bestScore = this.getPScore(sequence, 0);
      int bestIndex = 0;
      for (int i = 1; i < sequence.length() - this.length; i++){
        double currentScore = this.getPScore(sequence, i);
        if (currentScore > bestScore){
          bestScore = currentScore;
          bestIndex = i;
        }
      }
      return bestIndex;
    }

    public int length() {
        return this.length();
    }

    public Sequence mostProbable(){
      Sequence mostProbable = new Sequence( symbols );
      for (int i = 0; i < values.length; i++){
        double max = 0;
        int maxIndex = 0;
        for (int j = 0; j < values[i].length; j++){
          if (values[i][j] > max){
            max = values[i][j];
            maxIndex = j;
          }
        }
        mostProbable.append(symbols.get(maxIndex));
      }
      return mostProbable;
    }

}
