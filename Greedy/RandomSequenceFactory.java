import java.util.*;

public class RandomSequenceFactory {

    protected Random ran;
    protected SymbolSet symbols;

    public RandomSequenceFactory(SymbolSet s) {
        symbols  = s;
        ran = new Random();
        //ran.setSeed(123457);
    }

    public RandomSequenceFactory(SymbolSet s, int seed) {
        symbols  = s;
        ran = new Random();
        ran.setSeed(seed);
    }

    public Sequence generateSequence(int length) {
        Sequence theSeq = new Sequence(symbols);
        for (int i=0; i<length; i++) {
            char c = symbols.get(ran.nextInt(symbols.size()));
            theSeq.append(c);
        }
        return theSeq;
    }

    public SymbolSet getSymbolSet() {
        return symbols;
    }
}
