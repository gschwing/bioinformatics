import java.util.*;

public class Sequence {

    protected ArrayList<Character> sequence;
    protected SymbolSet symbols;

    public Sequence(SymbolSet s) {
        sequence = new ArrayList<Character>();
        symbols  = s;
    }

    public void append(Character c) {
        if (symbols.contains(c))
            sequence.add(c);
    }

    public int length() {
        return sequence.size();
    }

    public Character get(int index) {
        return sequence.get(index);
    }

    public Sequence getSubsequence(int startIndex, int length) {
        Sequence s = new Sequence(symbols);
        for (int i= startIndex; i < startIndex+length; i++)
            s.append(sequence.get(i));
        return s;
    }

    public SymbolSet getSymbolSet() {
        return symbols;
    }

    public String toString() {
        String returnval = "";
        for (char c : sequence)
            returnval += c;
        return returnval;
    }

    public void insertMotif(Motif motif, int startIndex){
      if (motif.length() > this.length()){
        System.out.println("Error: Motif length is greater than sequence.");
      } else if (motif.length() + startIndex > this.length()){
        System.out.println("Error: Index too high for motif.");
      } else {
        for (int i = startIndex; i < startIndex + motif.length(); i++){
          this.sequence.set(i, motif.get(i - startIndex));
        }
      }
    }
}
