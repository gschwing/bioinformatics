public class TestSequence {

    public static void main(String[] args) {

        Sequence foo = new Sequence( new DNASymbolSet() );
        foo.append('A');
        foo.append('C');
        foo.append('T');
        foo.append('G');
        foo.append('A');
        foo.append('C');
        foo.append('T');
        foo.append('G');
        System.out.println(foo);
        Sequence subFoo = foo.getSubsequence(2,3);
        System.out.println(subFoo);

    }
}
