import java.util.*;

public class Profile {
	
	protected int[] startIndices;
    protected double[][] values;
    protected int[][] totals;
    protected int[] columnTotals;
	protected SymbolSet symbols;
    protected int length;
	protected int hammingDistance;
	
	public Profile(ArrayList<Sequence> sequences, int[] startIndices, int length ) {
        this.startIndices = startIndices;
		this.length = length;
        symbols = sequences.get(0).getSymbolSet(); // TODO:DONE: Should check that each sequence is based on same SymbolSet.
	
	// Iterate through sequences ArrayList to check for consistent SymbolSet types
	ListIterator<Sequence> litr = sequences.listIterator();
	while(litr.hasNext()) {
		if(!( litr.next().getSymbolSet().equals(symbols))){		
			// Data is inconsistent, terminate and print error message
			System.out.println("Element " + litr.nextIndex() + " is of the wrong SymbolSet");
			System.exit(0);
		}
	}		
	
	// first, set up the values array, and a counter array of the same size
	int symbolSetSize = sequences.get(0).getSymbolSet().size();
        this.values = new double[length][symbolSetSize];
        totals = new int[length][symbolSetSize];
        for (int i=0; i<length; i++) {
            for (int j=0; j< symbolSetSize; j++) {
                this.values[i][j] = 0.0d;
                totals[i][j] = 0;
            }
        }

	// Check to see that startIndices length == Sequences length
	if(startIndices.length != sequences.size()){
		System.out.println("Usage Error: Class Profile.java: Number of Sequences must equal number of Start Indices");
		System.exit(1);
	}
        // now, loop over the sequences and accumulate totals of each symbol
        for (int i=0; i<sequences.size(); i++) {    // loop over sequences
            for (int j=0; j<length; j++) {          // loop over symbols in subsequence
                int indexOfSymbol = symbols.getIndex(sequences.get(i).get(j+startIndices[i]));
                totals[j][indexOfSymbol]++;
            }  // end loop over subsequence
        }  // end loop of sequences

        // now, finally, loop over the totals matrix to calculate totals per position
        columnTotals = new int[length]; // build and then initialize columnTotalsArray
        for (int i=0; i< length; i++)
            columnTotals[i] = 0;

        for (int i=0; i< length; i++) {
            for (int j=0; j< symbolSetSize; j++) {
               columnTotals[i] += totals[i][j];
            }
        }

        for (int i=0; i< length; i++) {
            for (int j=0; j< symbolSetSize; j++) {
               values[i][j] = (double)totals[i][j] / columnTotals[i];
            }
        }
	
    }
	
	public int hammingDistance(){
		int hammingDistance = 0;
		for( int i = 0; i < this.length; i++){
			int max = 0;
			for( int j = 0; j < this.symbols.size(); j++){	
				if (totals[i][j] > max)
				max = totals[i][j];
			}
			hammingDistance += columnTotals[i]-max;
		}
		return hammingDistance;
	}

	public int[] getStartIndices(){
		return startIndices;
	}	

    //TODO: Implement toString method to print out matrix.
    public String toString() {
        String returnval = "";
	returnval += "\n";
	String title = "Profile Matrix";
	String indent = new String(new char[(this.length*3)-(title.length()/4)]).replace("\0", " ");
	indent += title;
	returnval+= indent;
	returnval += "\n\n";
		for (int i = 0; i < this.length; i++){
            	returnval += String.format("      " + i);
		}
		
          	returnval += "\n";
		for (int i = 0; i < this.symbols.size(); i++){
        	returnval += symbols.get(i) + "   ";
		for (int j = 0; j < this.length; j++){
            	returnval += String.format("%.2f" + "   ", values[j][i]);
          	}
          returnval += "\n";
        }
        return returnval;
    }
}
