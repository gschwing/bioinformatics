import java.util.*;

/**///////////////////////////
/* A Brute Force Motif Finder / Median String Program
/* Author: Gregory Schwing
/* allPossibleStartingIndices - an arraylist to hold the combinatorial number equal to (n - l + 1) ^ t
/* allPossibleMotifs - an arraylist to hold the combinatorial number equal to (4) ^ l
/* @param ArrayList<Sequence> sequences - the DNA input, formatted as an arraylist of Class Sequence objects
/* @param Motif motif - the  
////////////////////////////*/


public class BruteForceSearch{
	
	ArrayList<String> allPossibleStartingIndices;
	ArrayList<String> allPossibleMotifs;
	ArrayList<Sequence> sequences;
	SymbolSet symbols;
	Motif motif;
	int n; // length of sequences, assumed to be the same size
	int l; // length of motif
	int startIndexRange; // range of possible indices 
	int t;
	int progress;
	int total;	
	ProgressBar bar;
	long startTime;
	int fivePercentFin;	

	public BruteForceSearch(ArrayList<Sequence> sequences, Motif motif){
		this.allPossibleStartingIndices = new ArrayList<String>();
		this.allPossibleMotifs = new ArrayList<String>();
		this.sequences = sequences;
		this.motif = motif;
		this.t = sequences.size();
		this.n = sequences.get(0).length();
		this.l = motif.length();
		this.startIndexRange = this.n - this.l + 1;
		this.symbols = motif.getSymbolSet();
		this.progress = 0;
		this.total = (int)Math.pow(this.startIndexRange, this.t);
		this.fivePercentFin = total / 20;
	}
	

	public void generateMotifs(){									
		
		char[] set = new char[symbols.size()];
		for(int i = 0; i < set.length; i++){
			set[i] = symbols.get(i);
		}
		int k = motif.length();
		int n = symbols.size();        
        	generateMotifsRecursive(set, "", n, k);

	}
	
	public void generateMotifsRecursive(char[] set, String prefix, int n, int k){
		
		// Base case: k is 0, print prefix
        	if (k == 0) {
        	    allPossibleMotifs.add(prefix);
			System.out.println(prefix);        	
	
			return;
        	}
 
	        // One by one add all characters from set and recursively 
       		// call for k equals to k-1
       		for (int i = 0; i < n; ++i) {
             	
           	// Next character of input added
           	String newPrefix = prefix + set[i]; 
             	
           	// k is decreased, because we have added a new character
           	generateMotifsRecursive(set, newPrefix, n, k - 1); 

		}

	}
	
	public void generateIndices(){									
		
		String[] set = new String[this.startIndexRange];
		for(int i = 0; i < set.length; i++){
			set[i] = Integer.toString(i) + " ";
		}
		int k = this.t;
		int n = set.length;        
		bar = new ProgressBar();
		generateIndicesRecursive(set, "", n, k);
		
	}
	
	public void generateIndicesRecursive(String[] set, String prefix, int n, int k){
		
        	if (k == 0) {
		    allPossibleStartingIndices.add(prefix);
			if(progress == 0){
				startTime = System.nanoTime();
			}
			progress++;
			if(this.progress == this.fivePercentFin){
				long endTime = System.nanoTime();
				long duration = (endTime - startTime);
				duration = duration / 1000000000; // in s
				// 1 prefixes / # ms
				duration = duration * 20;
				System.out.println("Estimated Remaining Time: " + (duration / 60) + " minutes"); // minutes
				
			}

			bar.update(this.progress, this.total);
			return;
        	}
 
	        // One by one add all characters from set and recursively 
       		// call for k equals to k-1
       		for (int i = 0; i < n; ++i) {
             	
           	// Next character of input added
           	String newPrefix = prefix + set[i]; 
           	// k is decreased, because we have added a new character
           	generateIndicesRecursive(set, newPrefix, n, k - 1); 

		}
	}
	

	public Profile findBestProfile(){
		
		generateIndices();
		ListIterator<String> litr1 = allPossibleStartingIndices.listIterator(); 
		Profile profile;
		Profile bestProfile = null;
		int hammingDistance = 100000;
		int swap_variable;
		int[] StartingIndexSet;
		int motif_size = motif.length();
		while(litr1.hasNext()) {
			String[] tokens = litr1.next().split(" ");
			int[] ary = new int[tokens.length];
			int i = 0;
			for (String token : tokens){
    				ary[i++] = Integer.parseInt(token); 
			}
			profile = new Profile(sequences, ary, motif_size);
			
			if (hammingDistance > (swap_variable = profile.hammingDistance())){
				hammingDistance = swap_variable;
				bestProfile = profile;
			}
		}
		return bestProfile;
	}
}

