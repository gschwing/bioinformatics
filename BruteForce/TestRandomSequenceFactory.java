public class TestRandomSequenceFactory {

    public static void main(String[] args) {

        RandomSequenceFactory foo = new RandomSequenceFactory( new DNASymbolSet() );
        Sequence bob = foo.generateSequence(100);
        System.out.println(bob);
        Sequence alice = foo.generateSequence(100);
        System.out.println(alice);

    }


}
