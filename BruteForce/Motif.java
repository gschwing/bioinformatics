public class Motif extends Sequence {

  public Motif (String sequence, SymbolSet symbolSet){

    super(symbolSet);

    for (int i = 0; i < sequence.length(); i++){
      append(sequence.charAt(i));
    }
  }

}
