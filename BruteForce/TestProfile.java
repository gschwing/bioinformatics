import java.util.*;
public class TestProfile {

	public static void main( String[] args){
	// number of DNA strings
	int t = 10;	
        // DNA Sequence Factory
	RandomSequenceFactory foo = new RandomSequenceFactory( new DNASymbolSet() );
	
	ArrayList<Sequence> sequences = new ArrayList<Sequence>();
	//Populate Sequences ArrayList
	Sequence sequence = null;
	Motif motif = new Motif("TCGATCGA", new DNASymbolSet() ); 
	for(int i = 0; i < t; i ++){
		sequence = foo.generateSequence(100);
		sequence.insertMotif(motif, 3);
		sequences.add(sequence);
	} 
//	Prints the Sequences
//	ListIterator<Sequence> litr = sequences.listIterator(); 
//	while(litr.hasNext()) {
//	System.out.println(litr.next());
//	}		
	int[] Starting_Indices = new int[]{3, 3, 3, 3, 3, 3, 3, 3, 3, 3};
	int motif_length = 8;	

	Profile profile = new Profile(sequences, Starting_Indices, motif_length);	
	System.out.println(profile);
	System.out.println("Hamming Distance: " + profile.hammingDistance());
	}
}
