import java.util.*;

public class TestBrute{




	public static void main( String[] args){
		

	// number of DNA strings
	int t = 3;
        // DNA Sequence Factory
	RandomSequenceFactory foo = new RandomSequenceFactory( new DNASymbolSet() );
	
	ArrayList<Sequence> sequences = new ArrayList<Sequence>();
	//Populate Sequences ArrayList
	Sequence sequence = null;
	Motif motif = new Motif("TCGATCGA", new DNASymbolSet() ); 
	for(int i = 0; i < t; i ++){
		sequence = foo.generateSequence(100);
		sequence.insertMotif(motif, i*10);
		sequences.add(i, sequence);
	}

	BruteForceSearch brutus = new BruteForceSearch(sequences, motif);
//	brutus.generateMotifs();
//	brutus.generateIndices();	
	Profile profile = brutus.findBestProfile();	
	System.out.println(profile);
	System.out.println("Hamming Distance: "+profile.hammingDistance());
	System.out.println("\nStart Indices of motif: " + Arrays.toString(profile.getStartIndices()));
	System.out.print("\n\n");
	}
}
